/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control.gravi.modelo.pojo;

import java.util.List;

/**
 *
 * @author imjesr
 */
public class Empresa {

    private int empresaID;
    private String nombre;
    private boolean activo;
    private String RFC;
    private List<Sucursal> sucursales;

    public Empresa(int empresaID, String nombre, boolean activo, String RFC) {
        this.empresaID = empresaID;
        this.nombre = nombre;
        this.activo = activo;
        this.RFC = RFC;
    }

    public int getEmpresaID() {
        return empresaID;
    }

    public void setEmpresaID(int empresaID) {
        this.empresaID = empresaID;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public String getRFC() {
        return RFC;
    }

    public void setRFC(String RFC) {
        this.RFC = RFC;
    }

    public List<Sucursal> getSucursales() {
        return sucursales;
    }

    public void setSucursales(List<Sucursal> sucursales) {
        this.sucursales = sucursales;
    }
}
