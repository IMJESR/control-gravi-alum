/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control.gravi.vista.modeldata;

import control.gravi.modelo.pojo.Empleado;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author imjesr
 */
public class EmpleadoModelData extends AbstractTableModel {

    // Lista de objetos
    private List<Empleado> items;
    // Lista de columnas
    String columnas[] = {"No. Emp.", "Nombre", "Puesto"};
    // Lista de tipo de columnas (Primitivo u Objeto)
    Class<?> columnaTipos[] = {String.class, String.class, String.class};

    // Indicar la lista que vamos a mostrar (constructor)
    public EmpleadoModelData(List<Empleado> items) {
        this.items = items;
    }

    @Override
    public int getRowCount() {
        return items.size();
    }

    @Override
    public int getColumnCount() {
        return columnas.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        if (columnIndex == 0) {
            return items.get(rowIndex).getNumeroDeEmpleado();
        }
        if (columnIndex == 1) {
            return items.get(rowIndex).getNombre();
        }
        if (columnIndex == 2) {
            return items.get(rowIndex).getPuesto();
        }
        return null;
    }

    @Override
    public String getColumnName(int column) {
        return columnas[column];
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return columnaTipos[columnIndex];
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }
}
