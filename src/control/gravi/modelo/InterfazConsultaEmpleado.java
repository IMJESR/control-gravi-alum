/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control.gravi.modelo;

import control.gravi.modelo.pojo.Empleado;
import java.util.List;

/**
 *
 * @author imjesr
 */
public interface InterfazConsultaEmpleado {

    void callback(List<Empleado> empleados);
}
