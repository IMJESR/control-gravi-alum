/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control.gravi.modelo.pojo;

import control.gravi.modelo.Printable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author imjesr
 */
public class Departamento implements Printable {

    int departamentoID;
    String nombre;
    List<Empleado> empleados;
    Empleado director;
    float sueldoPredefinido;
    boolean activo;

    public Departamento() {
        empleados = new ArrayList<>();
    }

    public Departamento(int departamentoID, String nombre, Empleado director, float sueldoPredefinido, boolean activo) {
        this.departamentoID = departamentoID;
        this.nombre = nombre;
        this.director = director;
        this.sueldoPredefinido = sueldoPredefinido;
        this.activo = activo;
        empleados = new ArrayList<>();
    }

    public int getDepartamentoID() {
        return departamentoID;
    }

    public void setDepartamentoID(int departamentoID) {
        this.departamentoID = departamentoID;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<Empleado> getEmpleados() {
        return empleados;
    }

    public void setEmpleados(List<Empleado> empleados) {
        this.empleados = empleados;
    }

    public Empleado getDirector() {
        return director;
    }

    public void setDirector(Empleado director) {
        this.director = director;
    }

    public float getSueldoPredefinido() {
        return sueldoPredefinido;
    }

    public void setSueldoPredefinido(float sueldoPredefinido) {
        this.sueldoPredefinido = sueldoPredefinido;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    @Override
    public void print() {
        System.out.println("Departamento ID: " + departamentoID);
        System.out.println("Nombre: " + nombre);
    }
}
