/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control.gravi.modelo.factory;

import control.gravi.modelo.factory.contrato.DepartamentoDAO;
import control.gravi.modelo.factory.contrato.EmpleadoDAO;
import control.gravi.modelo.factory.json.local.JSONLocalDAOFactory;
import control.gravi.modelo.factory.json.remoto.JSONRemotoDAOFactory;

/**
 *
 * @author imjesr
 */
public abstract class DAOFactory {

    public enum Fuente {
        JSON_LOCAL(2),
        JSON_RED(21),
        SQL_SERVER(22),
        MY_SQL(23),
        WS(24);
        private int value;

        Fuente(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }

    }

    public abstract EmpleadoDAO getEmpleadoDAO();

    public abstract DepartamentoDAO getDepartamentoDAO();

    public static DAOFactory getDAOFactory(Fuente whichFactory) {
        switch (whichFactory) {
            case JSON_LOCAL:
                return new JSONLocalDAOFactory();
            case JSON_RED:
                return new JSONRemotoDAOFactory();
            case SQL_SERVER:
                return null;
            case MY_SQL:
                return null;
            case WS:
                return null;
            default:
                return null;
        }
    }
}
