/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control.gravi.modelo.pojo;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author imjesr
 */
public class Sucursal {

    private int sucursalID;
    private String nombre;
    private String direccion;
    private List<Departamento> departamentos;
    private boolean activo;

    public Sucursal() {
        departamentos = new ArrayList<>();
    }

    public Sucursal(int sucursalID, String nombre, String direccion, boolean activo) {
        this.sucursalID = sucursalID;
        this.nombre = nombre;
        this.direccion = direccion;
        this.activo = activo;
        departamentos = new ArrayList<>();
    }

    public int getSucursalID() {
        return sucursalID;
    }

    public void setSucursalID(int sucursalID) {
        this.sucursalID = sucursalID;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public List<Departamento> getDepartamentos() {
        return departamentos;
    }

    public void setDepartamentos(List<Departamento> departamentos) {
        this.departamentos = departamentos;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }
}
