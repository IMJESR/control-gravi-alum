/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control.gravi.modelo.factory.json.local;

import control.gravi.modelo.factory.DAOFactory;
import control.gravi.modelo.factory.contrato.DepartamentoDAO;
import control.gravi.modelo.factory.contrato.EmpleadoDAO;

/**
 *
 * @author imjesr
 */
public class JSONLocalDAOFactory extends DAOFactory {

    @Override
    public EmpleadoDAO getEmpleadoDAO() {
        return new JSONLocalEmpleadoDAO();
    }

    @Override
    public DepartamentoDAO getDepartamentoDAO() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
