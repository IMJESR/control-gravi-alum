/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control.gravi.modelo.pojo;

import java.util.Date;

/**
 *
 * @author imjesr
 */
public class Empleado {

    private int empleadoID;
    private int numeroDeEmpleado;
    private Date fechaDeIngreso;
    private float sueldo;
    private String puesto;
    private String nombre;
    private String apellidoPaterno;
    private String apellidoMaterno;
    private Date fehchaDeNacimiento;
    private boolean activo;
    private Usuario usuario;

    public Empleado() {
        this.empleadoID = (int) (Math.random() * 100);
    }

    public Empleado(int empleadoID, int numeroDeEmpleado, Date fechaDeIngreso, float sueldo, String puesto, String nombre, String apellidoPaterno, String apellidoMaterno, Date fehchaDeNacimiento, boolean activo, Usuario usuario) {
        this.empleadoID = empleadoID;
        this.numeroDeEmpleado = numeroDeEmpleado;
        this.fechaDeIngreso = fechaDeIngreso;
        this.sueldo = sueldo;
        this.puesto = puesto;
        this.nombre = nombre;
        this.apellidoPaterno = apellidoPaterno;
        this.apellidoMaterno = apellidoMaterno;
        this.fehchaDeNacimiento = fehchaDeNacimiento;
        this.activo = activo;
        this.usuario = usuario;
    }

    public int getEmpleadoID() {
        return empleadoID;
    }

    public void setEmpleadoID(int empleadoID) {
        this.empleadoID = empleadoID;
    }

    public int getNumeroDeEmpleado() {
        return numeroDeEmpleado;
    }

    public void setNumeroDeEmpleado(int numeroDeEmpleado) {
        this.numeroDeEmpleado = numeroDeEmpleado;
    }

    public Date getFechaDeIngreso() {
        return fechaDeIngreso;
    }

    public void setFechaDeIngreso(Date fechaDeIngreso) {
        this.fechaDeIngreso = fechaDeIngreso;
    }

    public float getSueldo() {
        return sueldo;
    }

    public void setSueldo(float sueldo) {
        this.sueldo = sueldo;
    }

    public String getPuesto() {
        return puesto;
    }

    public void setPuesto(String puesto) {
        this.puesto = puesto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    public Date getFehchaDeNacimiento() {
        return fehchaDeNacimiento;
    }

    public void setFehchaDeNacimiento(Date fehchaDeNacimiento) {
        this.fehchaDeNacimiento = fehchaDeNacimiento;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

}
