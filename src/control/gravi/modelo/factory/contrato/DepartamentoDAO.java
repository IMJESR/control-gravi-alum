/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control.gravi.modelo.factory.contrato;

import java.util.List;

/**
 *
 * @author imjesr
 */
public interface DepartamentoDAO {

    public Object obtener(String id);

    public List obtenerTodos();

    public Object guardar(Object object);

    public Object actualizar(Object object);

    public Object eliminar(String id);
}
