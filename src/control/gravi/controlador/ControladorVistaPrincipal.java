/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control.gravi.controlador;

import control.gravi.modelo.ModeloVistaPrincipal;
import control.gravi.modelo.pojo.Empleado;
import control.gravi.vista.VistaPrincipal;

/**
 *
 * @author imjesr
 */
public class ControladorVistaPrincipal {

    private ModeloVistaPrincipal modelo;
    private VistaPrincipal vista;

    public ControladorVistaPrincipal(ModeloVistaPrincipal modelo) {
        this.modelo = modelo;
        // Mostrar ventana principal
        vista = new VistaPrincipal(this, modelo);
        vista.setVisible(true);
        modelo.inicializar();
    }

    public void empleadoSeleccionado(int index) {
        modelo.empleadoSeleccionado(index);
    }

    public void abrirVistaNuevoEmpleado() {
        // Modelo
        //Controlador
        ControladorNuevoEmpleado controlador = new ControladorNuevoEmpleado(modelo);
    }

    public void eliminarSeleccionado() {
        modelo.eliminarEmpleadoSeleccionado();
    }

    public void actualizarEmpleado(Empleado empleadoNuevo) {
        modelo.actualizarEmpleado(empleadoNuevo);
    }
}
