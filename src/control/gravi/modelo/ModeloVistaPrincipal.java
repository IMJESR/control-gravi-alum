/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control.gravi.modelo;

import control.gravi.modelo.factory.DAOFactory;
import control.gravi.modelo.factory.contrato.EmpleadoDAO;
import control.gravi.modelo.pojo.Empleado;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author imjesr
 */
public class ModeloVistaPrincipal implements InterfazConsultaEmpleado {

    private List<MiObserver> observers;
    private String saludo = "No hay informacion";
    private List<Empleado> empleados;
    private Empleado empleadoSeleccionado;

    private DAOFactory daoFactory = DAOFactory.getDAOFactory(DAOFactory.Fuente.JSON_LOCAL);
    private EmpleadoDAO empleadoDAO = daoFactory.getEmpleadoDAO();

    public ModeloVistaPrincipal() {
        observers = new ArrayList<>();
        empleados = new ArrayList<>();

    }

    public void empleadoSeleccionado(int index) {
        if (empleados != null) {
            empleadoSeleccionado = empleados.get(index);
            notificarObservers();
        }
    }

    public Empleado getEmpleadoSeleccionado() {
        return empleadoSeleccionado;
    }

    public List<Empleado> getEmpleados() {
        return empleados;
    }

    public void inicializar() {
        cargarEmpleados();
        empleados = new ArrayList<>();
        notificarObservers();
    }

    private void cargarEmpleados() {
        // Sin Hilos
        //empleados = empleadoDAO.obtenerTodos();
        //System.out.println("Empleados recuperados: " + empleados.size());

        // Metodo 1: Extender clase Thread
        // Thread t = new Thread(new HiloConsultaEmpleados(empleadoDAO, this));
        // t.start();
        // Metodo 2: Implementar interfaz Runnable
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                // SE ejecutara en otro hilo
                empleados = empleadoDAO.obtenerTodos();
                notificarObservers();
            }
        });
        t.start();
    }

    public void agregarObserver(MiObserver observer) {
        if (observers != null) {
            if (observer != null) {
                observers.add(observer);
            } else {
                System.out.println("No se puede agregar un observer nulo");
            }
        } else {
            System.out.println("La lista de observers en nula, instanciela");
        }

    }

    public void eliminarObserver(MiObserver observer) {
        if (observers.contains(observer)) {
            observers.remove(observer);
        } else {
            System.out.println("No se puede eliminar observer, no existe");
        }

    }

    public void notificarObservers() {
        for (MiObserver observer : observers) {
            observer.updateView();
        }
    }

    public void agregarEmpleado(Empleado empleado) {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                empleadoDAO.guardar(empleado);
                cargarEmpleados();
            }
        });
        t.start();

    }

    public void eliminarEmpleadoSeleccionado() {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                empleadoDAO.eliminar(String.valueOf(empleadoSeleccionado.getEmpleadoID()));
                cargarEmpleados();
            }
        });
        t.start();
    }

    public void actualizarEmpleado(Empleado empleado) {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                empleado.setEmpleadoID(empleadoSeleccionado.getEmpleadoID());
                empleadoDAO.actualizar(empleado);
                cargarEmpleados();
            }
        });
        t.start();

    }

    public void obtenerEmpleado(Empleado empleado) {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                empleadoDAO.obtener(empleado.getEmpleadoID());
                cargarEmpleados();
            }
        });
        t.start();

    }

    @Override
    public void callback(List<Empleado> empleados) {
        this.empleados = empleados;
        notificarObservers();
    }
}
