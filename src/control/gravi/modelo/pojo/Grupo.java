/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control.gravi.modelo.pojo;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author imjesr
 */
public class Grupo {

    private int grupoID;
    private String nombre;
    private char logo[];
    private String direccion;
    private List<Empresa> empresas;

    public Grupo() {
        empresas = new ArrayList<>();
    }

    public Grupo(int grupoID, String nombre, String direccion) {
        this.grupoID = grupoID;
        this.nombre = nombre;
        this.direccion = direccion;
        empresas = new ArrayList<>();
    }

    public int getGrupoID() {
        return grupoID;
    }

    public void setGrupoID(int grupoID) {
        this.grupoID = grupoID;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public char[] getLogo() {
        return logo;
    }

    public void setLogo(char[] logo) {
        this.logo = logo;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public List<Empresa> getEmpresas() {
        return empresas;
    }

    public void setEmpresas(List<Empresa> empresas) {
        this.empresas = empresas;
    }

}
