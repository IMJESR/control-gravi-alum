/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control.gravi.modelo.pojo;

import java.util.Date;

/**
 *
 * @author imjesr
 */
public class Usuario {

    private int usuarioID;
    private String nombreDeUsuario;
    private String password;
    private Date fechaDeRegistro;

    public Usuario() {
    }

    public Usuario(int usuarioID, String nombreDeUsuario, String password, Date fechaDeRegistro) {
        this.usuarioID = usuarioID;
        this.nombreDeUsuario = nombreDeUsuario;
        this.password = password;
        this.fechaDeRegistro = fechaDeRegistro;
    }

    public int getUsuarioID() {
        return usuarioID;
    }

    public void setUsuarioID(int usuarioID) {
        this.usuarioID = usuarioID;
    }

    public String getNombreDeUsuario() {
        return nombreDeUsuario;
    }

    public void setNombreDeUsuario(String nombreDeUsuario) {
        this.nombreDeUsuario = nombreDeUsuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getFechaDeRegistro() {
        return fechaDeRegistro;
    }

    public void setFechaDeRegistro(Date fechaDeRegistro) {
        this.fechaDeRegistro = fechaDeRegistro;
    }

}
