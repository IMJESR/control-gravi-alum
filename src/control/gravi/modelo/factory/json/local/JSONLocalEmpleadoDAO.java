package control.gravi.modelo.factory.json.local;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import control.gravi.modelo.factory.contrato.EmpleadoDAO;
import control.gravi.modelo.pojo.Empleado;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author imjesr
 */
public class JSONLocalEmpleadoDAO implements EmpleadoDAO {

    private List<Empleado> empleados;
    private boolean actualizado;
    private boolean encontrado;

    public JSONLocalEmpleadoDAO() {
        //empleados = new ArrayList<>();
        leerArchivo();

    }

    @Override
    public Object guardar(Object object) {
        boolean agregado = empleados.add((Empleado) object);
        actualizarArchivo();
        return agregado;
    }

    @Override
    public Object eliminar(String id) {
        boolean eliminado = false;
        Empleado empleado = (Empleado) obtener(Integer.valueOf(id));
        eliminado = empleados.remove(empleado);
        actualizarArchivo();
        return eliminado;
    }

    private void actualizarArchivo() {
        guardarEnArchivo();
        leerArchivo();
    }

    @Override
    public Object obtener(int id) {
        Empleado empTemp = null;

        for (Empleado item : empleados) {
            if (item.getEmpleadoID() == id) {
                empTemp = item;
            }
        }
        return empTemp;
    }

    @Override
    public Object actualizar(Object object) {
        // TODO Actualizar empleado
        Empleado empleado = (Empleado) object;
        
        for (Empleado item : empleados) {
            if (item.getEmpleadoID() == empleado.getEmpleadoID()) {
                item.setFechaDeIngreso(empleado.getFechaDeIngreso());
                item.setNumeroDeEmpleado(empleado.getNumeroDeEmpleado());
                item.setNombre(empleado.getNombre());
                item.setApellidoPaterno(empleado.getApellidoPaterno());
                item.setApellidoMaterno(empleado.getApellidoMaterno());
                item.setPuesto(empleado.getPuesto());
                item.setSueldo(empleado.getSueldo());
                item.setUsuario(empleado.getUsuario());
                actualizado = true;
            }
        }
        actualizarArchivo();
        return actualizado;
    }

    @Override
    public List obtenerTodos() {
        return empleados;
    }

    private void guardarEnArchivo() {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String json = gson.toJson(empleados);

        try {
            FileWriter writer = new FileWriter("Empleados.json");
            writer.write(json);
            writer.close();
        } catch (IOException ex) {
            System.out.println("Error: " + ex);
        }
    }

    private void leerArchivo() {
        try {
            Type empleadoType = new TypeToken<List<Empleado>>() {
            }.getType();
            Gson gson = new Gson();
            JsonReader reader = new JsonReader(new FileReader("Empleados.json"));
            empleados = gson.fromJson(reader, empleadoType); // contains the whole reviews list
        } catch (FileNotFoundException ex) {
            Logger.getLogger(JSONLocalEmpleadoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        //System.out.println("Tamano de empleados: " + empleados.size());
    }

}
