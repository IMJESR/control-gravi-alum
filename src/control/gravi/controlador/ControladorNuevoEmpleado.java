/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control.gravi.controlador;

import control.gravi.modelo.ModeloVistaPrincipal;
import control.gravi.modelo.pojo.Empleado;
import control.gravi.vista.VistaNuevoEmpleado;

/**
 *
 * @author imjesr
 */
public class ControladorNuevoEmpleado {

    private ModeloVistaPrincipal modelo;
    private VistaNuevoEmpleado vista;

    public ControladorNuevoEmpleado(ModeloVistaPrincipal modelo) {
        this.modelo = modelo;
        vista = new VistaNuevoEmpleado(this, modelo);
        vista.setVisible(true);
    }

    public void guardarEmpleado(Empleado empleado) {
        modelo.agregarEmpleado(empleado);
    }

}
