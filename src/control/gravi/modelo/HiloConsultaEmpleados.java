/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control.gravi.modelo;

import control.gravi.modelo.factory.contrato.EmpleadoDAO;
import control.gravi.modelo.pojo.Empleado;
import java.util.List;

/**
 *
 * @author imjesr
 */
public class HiloConsultaEmpleados extends Thread {

    private EmpleadoDAO dao;
    private InterfazConsultaEmpleado modelo;

    public HiloConsultaEmpleados(EmpleadoDAO dao, InterfazConsultaEmpleado modelo) {
        this.dao = dao;
        this.modelo = modelo;
        
    }

    @Override
    public void run() {
        List<Empleado> empleados = dao.obtenerTodos();
        modelo.callback(empleados);
        
        System.out.println("Consulta finalizada");
        //TODO Regresar al modelo, la lista de empleados obtenidos
    }

}
